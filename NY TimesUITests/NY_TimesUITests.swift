//
//  NY_TimesUITests.swift
//  NY TimesUITests
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import XCTest

class NY_TimesUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testScrolling() {
        app.launch()
        
        sleep(10)
        
        let tableView = app.tables.element(boundBy: 0)
        tableView.swipeUp()
        tableView.swipeUp()
    }

    func testGoToDetail() {
        app.launch()
        
        sleep(10)
        
        let tableView = app.tables.element(boundBy: 0)
        let firstCell = tableView.cells.element(boundBy: 0)
        firstCell.tap()
        
        sleep(3)
        
        app.navigationBars.buttons.element(boundBy: 0).tap()
    }
    
}
