//
//  DataManager.swift
//  NY Times
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import UIKit
import Foundation

let apiKey = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5"

let baseURL = "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/"

class DataManager: NSObject {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    class var sharedInstance: DataManager {
        struct Singleton {
            static let instance = DataManager()
        }
        return Singleton.instance
    }
    
    override init() {
        super.init()
    }
    
    func getArticles(viewController: UIViewController, backInDays: Int, completionHandler:@escaping (_ success: Bool, _ mainAppData: [ArticleViewModel]?, _ error: String?) -> Void) {
        Networking.GET(requestUrl: String(backInDays) + ".json", parameters: ["api-key": apiKey], success: { (data) in
            do {
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(Json4Swift_Base.self, from: data as! Data)
                var ArticlesViewModel = [ArticleViewModel]()
                if let articlesArray = responseModel.results {
                    for article in articlesArray {
                        let articleViewModel = ArticleViewModel(article: article)
                        ArticlesViewModel.append(articleViewModel)
                    }
                }
                completionHandler(true, ArticlesViewModel, nil)
            }
            catch {
                completionHandler(false, nil, NSLocalizedString("An error occurred, please try again later.", comment: ""))
            }
        }) { (errorMessage) in
            completionHandler(false, nil, errorMessage)
        }
    }
    
}

