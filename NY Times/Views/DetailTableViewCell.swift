//
//  DetailTableViewCell.swift
//  NY Times
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var byLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var abstractLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(article: Results) {
        self.titleLabel.text = article.title
        self.byLabel.text = article.byline
        self.dateLabel.text = article.published_date
        self.abstractLabel.text = article.abstract
    }

}
