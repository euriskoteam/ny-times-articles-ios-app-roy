//
//  ArticleTableViewCell.swift
//  NY Times
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var byLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(vm: ArticleViewModel) {
        self.titleLabel.text =  vm.article.title
        self.dateLabel.text = vm.article.published_date
        self.byLabel.text = vm.article.byline
        self.detailLabel.text = vm.article.source
        
        if let medias = vm.article.media, medias.count > 0 {
            if let mediaMeta = medias[0].mediametadata, mediaMeta.count > 0 {
                if mediaMeta[0].url != nil {
                    Helper.setImage(imgView: self.imgView, url: URL(string: mediaMeta[0].url!)!)
                }
            }
        }
    }

}
