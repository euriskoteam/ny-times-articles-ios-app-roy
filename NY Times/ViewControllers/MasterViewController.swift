//
//  MasterViewController.swift
//  NY Times
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var data = [TableViewCellBuilder]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.tableView.tableFooterView = UIView()

        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refrech), for: UIControl.Event.valueChanged)

        if #available(iOS 11.0, *) {
           self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.white]
        } else {
            // Fallback on earlier versions
        }
        
        self.getArticles(backInDays: 7)
    }
    
    @objc func refrech() {
        self.getArticles(backInDays: 7)
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = data[indexPath.row] as! ArticleViewModel
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.article = object.article
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return data[indexPath.row].cellInstance(tableView, indexPath: indexPath)
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return data[indexPath.row].rowHeight
    }

    //MARK: - API CALL
    
    func getArticles(backInDays: Int) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        DataManager.sharedInstance.getArticles(viewController: self, backInDays: backInDays) { (success, viewModels, errorMessage) in
            UIApplication.shared.endIgnoringInteractionEvents()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            OperationQueue.main.addOperation({
                self.refreshControl?.endRefreshing()
            })
            if success {
                self.data = viewModels!
                OperationQueue.main.addOperation({
                    self.tableView.reloadData()
                })
            }
            else {
                Helper.showAlert(fromViewController: self, message: errorMessage!)
            }
        }
    }
    
}

