//
//  ArticleViewModel.swift
//  NY Times
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import UIKit

class ArticleViewModel: TableViewCellBuilder {
    
    var article: Results
    var rowHeight: CGFloat = 119
    
    init(article: Results) {
        self.article = article
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleTableViewCell
        
        cell.setup(vm: self)
        
        return cell
    }
}

