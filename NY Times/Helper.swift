//
//  Helper.swift
//  NY Times
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import UIKit
import Kingfisher

class Helper: NSObject {
    
    class func setImage(imgView: UIImageView, url: URL) {
        var imgView = imgView
        imgView.kf.indicatorType = .activity
        imgView.kf.setImage(
            with: url,
            placeholder: nil,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
    
    class func showAlert(fromViewController: UIViewController, message: String) {
        OperationQueue.main.addOperation {
            let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: "Okay", style: .default) { (action:UIAlertAction) in
                
            }
            alertController.addAction(ok)
            fromViewController.present(alertController, animated: true, completion: nil)
        }
    }
    
}
