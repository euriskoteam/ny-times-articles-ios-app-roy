# NY Times

Build a simple app to hit the NY Times Most Popular Articles API and show a list of articles, that shows details when items on the list are tapped (a typical master/detail app).
The application is developed with Swift 4.2 and Xcode 10.1 using MVVM design pattern

## Getting Started

You need the following:
- AppleID enrolled to Developer/Entreprise program

## Running the tests

Explain how to run the automated tests for this system

## UnitTests

1- A simple test, to test the Parsing function inside the app

## UI Automation Tests

1- A simple test, to test the scrolling inside the app
2- A simple test, to test the details screen of the app



