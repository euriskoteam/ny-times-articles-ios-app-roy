//
//  NY_TimesTests.swift
//  NY TimesTests
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 EURSK. All rights reserved.
//

import XCTest
@testable import NY_Times

class NY_TimesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testParsing() {
        var json = [String: Any]()
        if let path = Bundle.main.path(forResource: "response", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    // do stuff
                    json = jsonResult as [String: Any]
                    let data = try JSONSerialization.data(withJSONObject: json, options: [])
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(Json4Swift_Base.self, from: data)
                    let article = response.results![0]
                    XCTAssertEqual(article.title, "State of the Union Fact Check: What Trump Got Right and Wrong")
                    XCTAssertEqual(article.url, "https://www.nytimes.com/2019/02/05/us/politics/fact-check-state-of-the-union.html")
                }
            } catch {
                XCTFail(error.localizedDescription);
            }
        }
    }

}
